class listOfObj {
    public static void main(String[] args) {
        dog Fido = new dog();
        Fido.size = 12;
        Fido.breed = "terrier";
        Fido.name = "fido";
        Fido.bark();
        Fido.printInfo();

        dog[] myPups = new dog[4];
        myPups[0] = new dog();
        myPups[1] = new dog();
        myPups[2] = new dog();
        myPups[3] = Fido;

        myPups[0].name = "Cornwallace";
        myPups[1].name = "Jeff ";
        myPups[2].name = "Samuel ";
        myPups[3].name = "Librarain";

        int x = 0;
        while (x < myPups.length) {
            myPups[x].speak();
            x = x + 1;
        }
    }

}
