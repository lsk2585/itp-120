public class dog {
    int size;
    String name;
    String breed;

    void bark() {
        System.out.println("Bark noise");
    }

    void speak() {
        System.out.println(name);
    }

    void printInfo() {
        System.out.println(size + " " + name + " " + breed);
    }
}