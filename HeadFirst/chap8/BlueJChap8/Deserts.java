
/**
 * Write a description of class Deserts here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Deserts extends Food implements Frozen
{
    int sodiumContent = 0;
    int expirationDate = 12;
    
    public Deserts(String food, String foodType){
        if (foodType.equals("frozen")){
            changeExpDate();
        }
        setName(food);
       
    }
    
    public void changeExpDate(){
        expirationDate = expirationDate + 150;
    }

}
