
/**
 * Write a description of class Meat here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Meat extends Food implements Frozen
{
    int sodiumContent = 0;
    int expirationDate = 3;
    
    public Meat(String food, String foodType){
        if (foodType.equals("frozen")){
            changeExpDate();
        }
        setName(food);
        
    }
    
    public void changeExpDate(){
        expirationDate = expirationDate + 40;
    }
    
}
