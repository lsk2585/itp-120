import java.util.*;

/**
 * Abstract class Food - write a description of the class here
 *
 * @author (your name here)
 * @version (version number or date here)
 */
public class Food {
    // instance variables - replace the example below with your own
    public int barCode = (int) (1000 * Math.random() * Math.random());;
    public int Calories;
    public double Cost;
    public int expirationDate;
    public int sodiumContent;
    public String name;

    public void setName(String food) {
        name = food;
    }

    public void changeSodiumContent(int val) {
        sodiumContent = sodiumContent + val;

    }
    
    public void setExpDate(int expDate){
        expirationDate = expDate;
    }

}
