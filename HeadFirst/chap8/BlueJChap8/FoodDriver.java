
/**
 * Write a description of class FoodDriver here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class FoodDriver extends Food {
    public static void main(String[] args) {
        Food[] i = new Food[6];
        i[0] = new Fruit("Banana", "");
        i[1] = new Meat("Frozen Chicken", "frozen");
        i[2] = new Vegetable("Canned Peas", "canned");
        i[3] = new Bread("Pita");
        i[4] = new Dairy("Egg");
        i[5] = new Fish("Salmon", "frozen");
        for (int y = 0; y < 7; y++) {
            System.out.println("Barcode: " + i[y].barCode + "\n" + i[y].name + "\nExpDate:" + i[y].expirationDate
                    + "\nSodium Content " + i[y].sodiumContent + "\n "+ i[y].getClass() + "\n -----------------------------------------------");

        }
    }
}
