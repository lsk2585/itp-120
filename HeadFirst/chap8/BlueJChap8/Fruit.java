
/**
 * Write a description of class Fruit here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Fruit extends Vegetarian implements Frozen {
    int sodiumContent = 0;
    int expirationDate = 7;
    int val = 800;

    public Fruit(String food, String foodType) {
        if (foodType.equals("frozen")) {
            changeExpDate();
        }
        if (foodType.equals("canned")) {
            changeSodiumContent(val);
        }
        setName(food);

    }


    public void changeExpDate() {
        expirationDate = expirationDate + 100;
    }
}
