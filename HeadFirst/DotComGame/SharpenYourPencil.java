import java.lang.Math;
import java.util.Random;

class SharpenYourPencil {
public static void main(String[] args) {
 int numOfGuesses = 0;
 CliReader reader = new CliReader();
 SimpleDotCom theDotCom = new SimpleDotCom();
 int randomNum = (int) (Math.random() * 5);

 int[] locations = {randomNum, randomNum+1, randomNum+2};
 theDotCom.setLocationCells(locations);
 boolean isAlive = true;
 while(isAlive == true) {
 String guess = reader.input("enter a number");
 String result = theDotCom.checkYourself(guess);
 numOfGuesses++;
 if (result.equals("kill")) {
 isAlive = false;
 System.out.println("You took " + numOfGuesses + " guesses");
 } // close if
 } // close while
 }
}
