public class SimpleDotCom {
	int[] locationCells;
	int numOfHits;
	boolean[] hitList = new boolean[3];

	void setLocationCells(int[] locs) {
		locationCells = locs;
	}

	public String checkYourself(String stringGuess) {
		int index = 0;
		int guess = Integer.parseInt(stringGuess);
		String result = "miss";
		for (int cell : locationCells) {
			if (guess == cell) {
				if (hitList[index] == true) {
					result = "Guess a different number";
				}
				if (hitList[index] == false) {
					result = "hit";
					hitList[index]=true;
					numOfHits++;
				}
				break;

			}
			index++;

		}
		if (numOfHits == locationCells.length) {
			result = "kill";
		}
		System.out.println(result);
		return result;
	}

}
