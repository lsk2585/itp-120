import java.util.Random;
import java.lang.Math;

public class PlaySimpleDotCom {
    public static void main(String[] args) {
        SimpleDotCom game = new SimpleDotCom();
        int[] locations = new int[3];
        int initialLoc = (int) (Math.random() * (6));
        int pos = 0;
        for (int x : locations) {
            locations[pos] = initialLoc;
            initialLoc = initialLoc + 1;
            pos++;
        }
        game.setLocationCells(locations);
        CliReader reader = new CliReader();

        int numOfGuesses = 0;
        boolean loop = true;
        while (loop == true) {
            String guess = reader.input("Enter a guess ");
            String result = game.checkYourself(guess);
            if (result.equals("miss") || result.equals("hit")) {
                numOfGuesses++;
            } else if (result.equals("kill")) {
                numOfGuesses++;
                loop = false;
            }
        }
        System.out.println("It took you " + numOfGuesses + " guesses");

    }

}
