import java.util.*;

public class DotCom {
	private ArrayList<String> locationCells;
	String websiteName;

	public void setLocationCells(ArrayList<String> loc) {
		locationCells = loc;
	}

	public void setName(String title) {
		websiteName = title;
	}

	public String checkYourself(String userInput) {

		int index = locationCells.indexOf(userInput);
		System.out.println(index);
		if (index >= 0) {
			locationCells.remove(index);
			if (locationCells.isEmpty()) {
				System.out.println("Is empty");
			}
			return "hit";
		}
		return "miss";
	}
}
