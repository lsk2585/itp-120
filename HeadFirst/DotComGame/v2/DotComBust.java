import java.util.*;

public class DotComBust {

    private CliReader helper = new CliReader();
    private ArrayList<DotCom> dotComsList = new ArrayList<DotCom>();
    private int numOfGuesses = 0;

    public void setUpGame() {
        DotCom ship1 = new DotCom();
        ship1.setName("apsva.instructure.com");
        DotCom ship2 = new DotCom();
        ship2.setName("ict.gctaa.net/sections/itp120/");
        DotCom ship3 = new DotCom();
        ship3.setName("gitLab.com");
        Collections.addAll(dotComsList, ship1, ship2, ship3);

        System.out.println("Try to sink all three ships in as few guesses as possible.");
        System.out.println(
                "Your targets are " + "apsva.instructure.com" + "ict.gctaa.net/sections/itp120/" + "gitLab.com");

        for (DotCom dotComToSet : dotComsList) {
            ArrayList<String> newLocation = GameHelper.placeDotCom(3);
            dotComToSet.setLocationCells(newLocation);
        }
    }

    public void startPlaying() {
        while (!dotComsList.isEmpty()) {
            String userGuess = helper.input("Enter a guess ");
            checkUserGuess(userGuess);

        }
        finishGame();

    }

    public void checkUserGuess(String userGuess) {
        numOfGuesses++;
        String result = "miss";
        for (DotCom dotComToTest : dotComsList) {
            result = dotComToTest.checkYourself(userGuess);
            if (result.equals("hit")) {
                break;
            }

            if (result.equals("kill")) {
                dotComsList.remove(dotComToTest);
                break;
            }
        }
        System.out.println(result);
    }

    private void finishGame() {
        System.out.println("All the websites have sunk in price. The bubble has burst");
        if (numOfGuesses <= 18) {
            System.out.println("It only took you " + numOfGuesses + " guesses.");
            System.out.println("If only you'd shorted the stock");
        } else {
            System.out.println("Took you long enough " + numOfGuesses + " guesses.");
            System.out.println("There goes your kids college fund");

        }
    }

    public static void main(String[] args) {
        DotComBust game = new DotComBust();
        game.setUpGame();
        game.startPlaying();
    }
}
