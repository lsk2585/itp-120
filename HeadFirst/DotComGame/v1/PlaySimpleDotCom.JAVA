
/* It will compile without error, the code wil not output anything, it'll only create an object 
of the SimpleDotCom class and assing it go game */
/* Okay, nevermind it does not compile with error, 
it is missing a retun statement in the method checkYourself(), to fix this all i need to do is change it from return string to void  */
/* Compared to the solution provided by the textbook, 
I took a little more lazy coder automating something which ends up being 
harder to code than the simple solution, such as with writing a for loop to assign the 
array items to the random number generated incrementing by 1, I also used the CLI Reader for collecting user input, the way 
I generated my random number was also different, the condition of my if statement was also differnt, but besides that the programs were about the same*/

import java.util.Random;
import java.lang.Math;

public class PlaySimpleDotCom {
    public static void main(String[] args) {
        SimpleDotCom game = new SimpleDotCom();
        int[] locations = new int[4];
        int initialLoc = (int) (Math.random() * (6));
        System.out.println(initialLoc);
        int pos = 0;
        for (int x : locations) {
            locations[pos] = initialLoc;
            initialLoc = initialLoc + 1;
            // System.out.println(locations[pos]);
            pos++;
        }
        game.setLocationCells(locations);
        CliReader reader = new CliReader();

        int numOfGuesses = 0;
        boolean loop = true;
        while (loop == true) {
            String guess = reader.input("Enter a guess ");
            String result = game.checkYourself(guess);
            if (result.equals("miss") || result.equals("hit")) {
                numOfGuesses++;
            } else {
                loop = false;
            }
        }
        System.out.println("It took you " + numOfGuesses + " guesses");

    }
}
