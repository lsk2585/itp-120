class Puzzle4b {
    int ivar;

    public int doStuff(int factor) {
        if (ivar > 100) {
            return ivar + factor;
        } else {
            return ivar * factor;
        }
    }
}