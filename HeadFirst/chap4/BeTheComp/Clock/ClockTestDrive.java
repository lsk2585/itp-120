/*the program will not print out "time: 1245" because the getTime() is 
defined as a function void that does not return a value, to fix change return value
to String
*/
class ClockTestDrive {
    public static void main(String[] args) {
        Clock c = new Clock();
        c.setTime("1245");
        String tod = c.getTime();
        System.out.println("time: " + tod);
    }
}