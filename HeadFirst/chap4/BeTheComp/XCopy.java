/*this program will run, it will print out 42 because the function does not manipulate the value orig 
when it's placed in as an argument, it returns 2*42 which is 84, printing oyt 42 space 84
*/
class XCopy {
	public static void main(String[] args) {
		int orig = 42;
		XCopy x = new XCopy();
		int y = x.go(orig);
		System.out.println(orig + " " + y);

	}

	int go(int arg) {
		arg = arg * 2;
		return arg;
	}
}