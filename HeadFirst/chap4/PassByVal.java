class PassByVal {

    public static void main(String[] args) {
        int x = 7;

        System.out.println("x in main method " + x);
        go(x);
        if (x == 7) {
            System.out.println(
                    "Despite me being passed in as a parameter and the method changing the value of x, I'm still the same!!");
        }

    }

    public static final int go(int x) {
        x = 0;
        System.out.println("x in go() method " + x);
        return x;
    }

}
