import java.util.*;
import static java.lang.System.out; //lets you avoid typing System

class FullMoons {
    static int Day_IM = 1000 * 60 * 60 * 24; //converts a day to milliseconds

    public static void main(String[] args) {
        Calendar C = Calendar.getInstance(); 
        /*you can't initialize an abstract object, 
        but can the instance of a concrete subclass, which is why it is .getInstance();*/
        C.set(2004, 0, 7, 15, 40); //sets the current date
        long day1 = C.getTimeInMillis(); //converts date to milliseconds
        for (int x = 0; x < 60; x++) //for 60 months run loop{
            day1 += (Day_IM * 29.52); //adds 29.52 days to the prev date
            C.setTimeInMillis(day1); //sets date to new date
            out.println(String.format("full moon on %tc", C)); //prints out date in specific date time

    }
}