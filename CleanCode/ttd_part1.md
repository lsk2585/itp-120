What are the Three Laws of Test-driven Development?
1. You are not allowed to write any production code until you've got a unit test faling first because production code is not written yet
2. Stop writing the test as soon as it fails even if it's a compiler error, don't write more of test than is sufficent to fail even if failure it just to compile, as soon as a test fails for any reason stop writing the test and start writing production code that'll make it pass
3. Stop writing the production code when it passes the test, and then go back to writing the test, and repeat this cycle until you're done
Explain the Red -> Green -> Refactor -> Red process.
Red) First make test fail Green) Make test pass Refactor) Clean up code, often, doesn't appear on schedule -> repeat
What are the three characteristics Uncle Bob lists of rotting code? (Note: these were covered in Episode 1).
Rigid, Fragile, Immobile, codes get tangled, warped and perverted, debugging become complicated. 
Explain how fear promotes code rot and why the fear exists in the first place. How does TDD help us break this vicious cycle?
If you attempt to clean up rotted code you may break it remphasizing code rot to continue, the fear exists in the first place, when you break the code it become your code. TDD helps us break the viscious cycle by emphasizing iterative development with small imporvements forced to meet increases in small tests, and then emphasizes refacotring of the code to keep it clean.

Uncle Bob mentions FitNesse as an example of a project that uses TDD effectively. What is FitNesse? What does it do?
It's a sizeable Open source project, which has tests that run in under a minute and if it passes the test then the code will be shipped.
What does Uncle Bob say about a program with a long bug list? What does he say this comes from?
It comes with iressponsibility and carelessness.
What two other benefits does Uncle Bob say you get from TDD besides a reduction in debugging time?

