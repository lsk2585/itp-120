import java.util.*;

public class Houses {
    static int teamChecker = 0;
    static int teamIndex = 0;
    static String Gryf = "Gryffindor";
    static String Rave = "Ravenclaw";
    static String Slyth = "Slytherin";
    static String Huffle = "Hufflepuff";
    static ArrayList<String> teamNames = new ArrayList<String>();
    static ArrayList<ArrayList<String>> Teams = new ArrayList<ArrayList<String>>();
    static ArrayList<String> Gryffindor = new ArrayList<String>();
    static ArrayList<String> Ravenclaw = new ArrayList<String>();
    static ArrayList<String> Slytherin = new ArrayList<String>();
    static ArrayList<String> Hufflepuff = new ArrayList<String>();

    public static void addPlayer(String name, ArrayList team) {
        team.add(name);
    }

    public static ArrayList strArrLis(String team) {
        if (team.equals("Gryffindor")) {
            return Gryffindor;
        }

        else if (team.equals("Ravenclaw")) {
            return Ravenclaw;
        }

        else if (team.equals("Slytherin")) {
            return Slytherin;
        }

        else {
            return Hufflepuff;
        }

    }

    public static void checkLength() {
        Teams.add(Gryffindor);
        Teams.add(Ravenclaw);
        Teams.add(Slytherin);
        Teams.add(Hufflepuff);

        teamNames.add(Gryf);
        teamNames.add(Rave);
        teamNames.add(Slyth);
        teamNames.add(Huffle);

        Teams.forEach(item -> {
            if (Teams.get(teamIndex).size() > 7) {
                System.out.println(teamNames.get(teamIndex) + " has too many players");
            } else if (Teams.get(teamIndex).size() < 7) {
                System.out.println(teamNames.get(teamIndex) + " does not have enough players");
            } else {
                teamChecker++;

            }
            teamIndex++;
        });
        if (teamChecker == 4) {
            System.out.println("List complete, let’s play quidditch!");
        }
    }

}