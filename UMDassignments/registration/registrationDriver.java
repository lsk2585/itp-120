import java.util.*;

public class registrationDriver {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] player = line.split(",");
            String name = player[0];
            String[] team = player[1].split(" ");
            Houses.addPlayer(name, Houses.strArrLis(team[team.length - 1]));
        }
        Houses.checkLength();
    }
}