import java.math.BigDecimal;
import java.util.*;
import java.math.RoundingMode;

class simpleGeneSplice {

    static int mix(String amntA, String amntB, String C, String tolerance) {

        BigDecimal a = new BigDecimal(amntA);
        BigDecimal b = new BigDecimal(amntB);
        BigDecimal amntC = new BigDecimal(C);
        BigDecimal perfMix = a.divide(b);
        BigDecimal origAmntBDish1 = new BigDecimal("0");

        BigDecimal origAmntBDish2 = new BigDecimal(amntB);
        BigDecimal origAmntADish1 = new BigDecimal(amntA);
        BigDecimal origAmntADish2 = new BigDecimal("0");

        // (perfMix + tolerance <= origAmntADish2 / origAmntBDish2
        // || perfMix - tolerance >= origAmntADish2 / origAmntBDish2)
        // && (perfMix + tolerance <= origAmntADish1 / origAmntBDish1
        // || perfMix - tolerance >= origAmntADish1 / origAmntBDish1)
        /*
         * (perfMix - (origAmntADish2 / origAmntBDish2) < tolerance || perfMix -
         * (origAmntADish2 / origAmntBDish2) > tolerance) && ((perfMix - (origAmntADish1
         * / origAmntBDish1) < tolerance) || (perfMix - (origAmntADish1 /
         * origAmntBDish1) > tolerance))
         */

        // while((perfMix - tolerance > (origAmntADish1 / origAmntBDish1)
        // || (perfMix + tolerance < (origAmntADish1 / origAmntBDish1))
        // && (perfMix - tolerance > (origAmntADish2 / origAmntBDish2)
        // || (perfMix + tolerance < (origAmntADish2 / origAmntBDish2)))))
        int I = 0;
        for (int i = 0; i < 93; i++) {

            System.out.println("origAmntADish1: " + origAmntADish1);
            System.out.println("origAmntBDish1: " + origAmntBDish1);
            // System.out.println("ratioDish1: " + (origAmntADish1.divide(origAmntBDish1, 2,
            // RoundingMode.HALF_EVEN)));
            System.out.println("origAmntADish2: " + origAmntADish2);
            System.out.println("origAmntBDish2: " + origAmntBDish2);
            // System.out.println("ratioDish2: " + (origAmntADish2.divide(origAmntBDish2, 2,
            // RoundingMode.HALF_EVEN)));
            System.out.println(" ");

            BigDecimal oldAmntBDish1 = (origAmntBDish1.subtract(amntC
                    .multiply(origAmntBDish1.divide(origAmntBDish1.add(origAmntADish1), 2, RoundingMode.HALF_EVEN))));
            BigDecimal oldAmntBDish2 = (origAmntBDish2.add((amntC
                    .multiply(origAmntBDish1.divide(origAmntBDish1.add(origAmntADish1), 2, RoundingMode.HALF_EVEN)))));
            BigDecimal oldAmntADish1 = (origAmntADish1.subtract(amntC
                    .multiply(origAmntADish1.divide(origAmntBDish1.add(origAmntADish1), 2, RoundingMode.HALF_EVEN))));
            BigDecimal oldAmntADish2 = (origAmntADish2.add(amntC
                    .multiply(origAmntADish1.divide(origAmntBDish1.add(origAmntADish1), 2, RoundingMode.HALF_EVEN))));

            System.out.println("oldAmntADish1: " + oldAmntADish1);
            System.out.println("oldAmntBDish1: " + oldAmntBDish1);
            System.out.println("ratioDish1: " + (oldAmntADish1.divide(oldAmntBDish1, 2, RoundingMode.HALF_EVEN)));
            System.out.println("oldAmntADish2: " + oldAmntADish2);
            System.out.println("oldAmntBDish2: " + oldAmntBDish2);
            System.out.println("ratioDish2: " + (oldAmntADish2.divide(oldAmntBDish2, 2, RoundingMode.HALF_EVEN)));
            System.out.println(" ");

            BigDecimal newAmntBDish1 = (oldAmntBDish1.add(
                    amntC.multiply(oldAmntBDish1.divide(oldAmntBDish1.add(oldAmntADish1), 2, RoundingMode.HALF_EVEN))));
            BigDecimal newAmntBDish2 = (oldAmntBDish2.subtract((amntC
                    .multiply(oldAmntBDish1.divide(oldAmntBDish1.add(oldAmntADish1), 2, RoundingMode.HALF_EVEN)))));
            BigDecimal newAmntADish1 = (oldAmntADish1.add((amntC
                    .multiply(oldAmntADish1.divide(oldAmntBDish1.add(oldAmntADish1), 2, RoundingMode.HALF_EVEN)))));
            BigDecimal newAmntADish2 = (oldAmntADish2.subtract(
                    amntC.multiply(oldAmntADish1.divide(oldAmntBDish1.add(oldAmntADish1), 2, RoundingMode.HALF_EVEN))));

            System.out.println("newAmntADish1: " + newAmntADish1);
            System.out.println("newAmntBDish1: " + newAmntBDish1);
            System.out.println("ratioDish1: " + (newAmntADish1.divide(newAmntBDish1, 2, RoundingMode.HALF_EVEN)));
            System.out.println("newAmntADish2: " + newAmntADish2);
            System.out.println("newAmntBDish2: " + newAmntBDish2);
            System.out.println("ratioDish2: " + (newAmntADish2.divide(newAmntBDish2, 2, RoundingMode.HALF_EVEN)));
            System.out.println(" ");

            origAmntADish1 = newAmntADish1;
            origAmntADish2 = newAmntADish2;
            origAmntBDish1 = newAmntBDish1;
            origAmntBDish2 = newAmntBDish2;
            I++;
            System.out.println("Number of iterations so far " + I);

            // if (perfMix - tolerance > (origAmntADish1 / origAmntBDish1){
            // System.out.println("true");
            // }

            // if (perfMix + tolerance < (origAmntADish1 / origAmntBDish1))

            // (perfMix - tolerance > (origAmntADish2 / origAmntBDish2)

            // (perfMix + tolerance < (origAmntADish2 / origAmntBDish2)

            System.out.println("-----------------------------");

        }
        // System.out.println(origAmntADish1 / origAmntBDish1);
        // System.out.println(origAmntADish2 / origAmntBDish2);

        return I;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner("12 300 20 2 0.01");

        int testCases = scanner.nextInt();
        int i = 0;
        while (i < testCases) {
            String amntA = scanner.next();
            String amntB = scanner.next();
            String amntC = scanner.next();
            String tol = scanner.next();

            System.out.println(mix(amntA, amntB, amntC, tol));
            i++;

        }
    }
}