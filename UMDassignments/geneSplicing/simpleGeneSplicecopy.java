import java.util.*;

class simpleGeneSplicecopy {

    static int mix(double amntA, double amntB, double amntC, double tolerance) {
        double perfMix = (amntA / amntB);

        double origAmntBDish1 = 0; // no liquid B intially in dish A
        double origAmntBDish2 = amntB;
        double origAmntADish1 = amntA;
        double origAmntADish2 = 0; // no liquid A initially in dish B

        // for (int i = 0; i < 93; i++)

        int I = 0;

        // once ratio of petri dish 1 and 2 is within tolerance of perfect ratio stop,
        // when perfmix - tol < ratio < perfmix + tol stop
        while ((perfMix - tolerance > (origAmntADish1 / origAmntBDish1)
                || (perfMix + tolerance < (origAmntADish1 / origAmntBDish1))
                        && (perfMix - tolerance > (origAmntADish2 / origAmntBDish2)
                                || (perfMix + tolerance < (origAmntADish2 / origAmntBDish2))))) {
            I++; // Number of iterations incramenter

            /*
             * print statements for debugging System.out.println("origAmntADish1: " +
             * origAmntADish1); System.out.println("origAmntBDish1: " + origAmntBDish1);
             * System.out.println("ratioDish1: " + (origAmntADish1 / origAmntBDish1));
             * System.out.println("origAmntADish2: " + origAmntADish2);
             * System.out.println("origAmntBDish2: " + origAmntBDish2);
             * System.out.println("ratioDish2: " + (origAmntADish2 / origAmntBDish2));
             * System.out.println("liquidDishA: " + (origAmntADish1 + origAmntBDish1));
             * System.out.println("liquidDishB: " + (origAmntADish2 + origAmntBDish2));
             * System.out.println(" ");
             */

            // step 1: remove amount c from dish1 and add to dish2 in respective ratio

            double oldAmntBDish1 = (origAmntBDish1 - ((amntC * (origAmntBDish1 / (origAmntBDish1 + origAmntADish1)))));
            double oldAmntBDish2 = (origAmntBDish2 + ((amntC * (origAmntBDish1 / (origAmntBDish1 + origAmntADish1)))));
            double oldAmntADish1 = (origAmntADish1 - (amntC * (origAmntADish1 / (origAmntBDish1 + origAmntADish1))));
            double oldAmntADish2 = (origAmntADish2 + (amntC * (origAmntADish1 / (origAmntBDish1 + origAmntADish1))));

            /*
             * print statements for debugging System.out.println("oldAmntADish1: " +
             * oldAmntADish1); System.out.println("oldAmntBDish1: " + oldAmntBDish1);
             * System.out.println("ratioDish1: " + (oldAmntADish1 / oldAmntBDish1));
             * System.out.println("oldAmntADish2: " + oldAmntADish2);
             * System.out.println("oldAmntBDish2: " + oldAmntBDish2);
             * System.out.println("ratioDish2: " + (oldAmntADish2 / oldAmntBDish2));
             * System.out.println("liquidDishA: " + (oldAmntADish1 + oldAmntBDish1));
             * System.out.println("liquidDishB: " + (oldAmntADish2 + oldAmntBDish2));
             * System.out.println(" ");
             */

            /*
             * step 2 : remove amount c from dish2 and add to dish1 in respective ratio, if
             * you continue to repreat these steps for a very long time the ratio of amntA
             * to amntB in both petri dishes should be the same as the original ratio of
             * amntA to amntB
             */
            double newAmntBDish1 = (oldAmntBDish1 + ((amntC * (oldAmntBDish2 / (oldAmntBDish2 + oldAmntADish2)))));
            double newAmntBDish2 = (oldAmntBDish2 - ((amntC * (oldAmntBDish2 / (oldAmntBDish2 + oldAmntADish2)))));
            double newAmntADish1 = (oldAmntADish1 + (amntC * (oldAmntADish2 / (oldAmntBDish2 + oldAmntADish2))));
            double newAmntADish2 = (oldAmntADish2 - (amntC * (oldAmntADish2 / (oldAmntBDish2 + oldAmntADish2))));

            /*
             * print statements for debugging System.out.println("newAmntADish1: " +
             * newAmntADish1); System.out.println("newAmntBDish1: " + newAmntBDish1);
             * System.out.println("ratioDish1: " + (newAmntADish1 / newAmntBDish1));
             * System.out.println("newAmntADish2: " + newAmntADish2);
             * System.out.println("newAmntBDish2: " + newAmntBDish2);
             * System.out.println("ratioDish2: " + (newAmntADish2 / newAmntBDish2));
             * System.out.println("liquidDishA: " + (newAmntADish1 + newAmntBDish1));
             * System.out.println("liquidDishB:  " + (newAmntADish2 + newAmntBDish2));
             * System.out.println(" ");
             */

            origAmntADish1 = newAmntADish1;
            origAmntADish2 = newAmntADish2;
            origAmntBDish1 = newAmntBDish1;
            origAmntBDish2 = newAmntBDish2;

            // System.out.println("Number of iterations so far " + I);

            // System.out.println("-----------------------------");

        }

        return I;
    }

    public static void main(String[] args) {
        // takes in test data file
        Scanner scanner = new Scanner(System.in);

        int testCases = scanner.nextInt();
        // parses each case as double
        int i = 0;
        while (i < testCases) {
            double amntA = Double.parseDouble(scanner.next()); // amount of liquid DNA of A in Dish 1
            double amntB = Double.parseDouble(scanner.next()); // amount of liquid DNA of B in Dish 2
            double amntC = Double.parseDouble(scanner.next()); // amount of liquid DNA transfered in each step
            double tol = Double.parseDouble(scanner.next()); // tolerance of perfect ratio, +- tolerance of ratio a/b
            System.out.println(mix(amntA, amntB, amntC, tol)); // function to mix DNA until within tolerance of perfect
                                                               // ratio
            i++;

        }
    }
}