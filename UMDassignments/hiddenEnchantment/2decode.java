import java.util.*;

class decode2 {
    static String passage = "ThisIsTheOneRing";
    static String enchantement = "OneRingToFindThem";
    static char[] pasChar = passage.toCharArray();
    static char[] enChar = enchantement.toCharArray();

    public static void main(String[] args) {
        System.out.println("  ");
        int e = 0;
        int lenCont = 0;
        for (int index : pasChar) {
            System.out.print(pasChar[e]);
            lenCont++;
            e++;
        }
        System.out.println("  ");

        for (int j = 0; j < lenCont; j++) {
            System.out.print("-");
        }
        System.out.println("  ");
        int maRowCo = 0;
        int i = 0;
        for (int each : enChar) {
            System.out.println(enChar[i] + "|");
            int b = 0;
            for (int index : pasChar) {
                if ((pasChar[i]) == (enChar[b])) {
                    System.out.print("x");
                    maRowCo++;
                } else if ((pasChar[i] == (enChar[b])) && (maRowCo >= 3)) {
                    System.out.print("Q");
                    maRowCo++;
                } else {
                    System.out.print(".");
                    maRowCo = 0;
                }

                b++;
            }

            i++;
        }
    }
}