import java.util.*;

class decode2 {
    static String passage = "OneRingToRuleThemAll,OneRingToFindThem";
    static String enchantement = "OneRingToBringThemAll,AndInTheDarknessBindThem";
    // static String passage = "";
    // static String enchantement = "grinning";
    static char[] pasChar = passage.toCharArray();
    static char[] enChar = enchantement.toCharArray();

    public static void main(String[] args) {
        System.out.print("  ");
        int e = 0;
        int lenCont = 0;
        for (int index : pasChar) {
            System.out.print(pasChar[e]);
            lenCont++;
            e++;
        }
        System.out.println("");
        System.out.print("  ");

        for (int j = 0; j < lenCont; j++) {
            System.out.print("-");
        }
        int i = 0;
        for (int each : enChar) {
            System.out.println("");
            System.out.print(enChar[i] + "|");
            int b = 0;
            for (int index : pasChar) {
                if ((pasChar[b]) == (enChar[i])) {
                    int starter = -2;
                    int end = 3;
                    // if at or close to beggining due to being out of index if not
                    if (i == 0 || b == 0) {
                        starter = 0;
                        end = 5;
                    }
                    if (i == 1 || b == 1) {
                        starter = -1;
                        end = 4;
                    }
                    /*
                     * if ((i == 0 && (b == enChar.length - 1))) {
                     * 
                     * } if ((i == 0 && (b == enChar.length - 2))) {
                     * 
                     * } if ((i == 0 && (b == enChar.length - 3))) {
                     * 
                     * } if ((i == 1 && (b == enChar.length - 1))) {
                     * 
                     * } if ((i == 1 && (b == enChar.length - 2))) {
                     * 
                     * } if ((i == 1 && (b == enChar.length - 3))) {
                     * 
                     * }
                     * 
                     * if ((i == 0 && (b == pasChar.length - 1))) {
                     * 
                     * } if ((i == 0 && (b == pasChar.length - 2))) {
                     * 
                     * } if ((i == 0 && (b == pasChar.length - 3))) {
                     * 
                     * } if ((i == 1 && (b == pasChar.length - 1))) {
                     * 
                     * } if ((i == 1 && (b == pasChar.length - 2))) {
                     * 
                     * } if ((i == 1 && (b == pasChar.length - 3))) {
                     * 
                     * }
                     */

                    // if at or close to beginning due to being out of index if not

                    if (i == (pasChar.length - 1) || (b == enChar.length - 1)) {
                        end = 0;

                    }
                    if (i == (pasChar.length - 2) || (b == enChar.length - 2)) {
                        end = 1;

                    }
                    if (i == (pasChar.length - 3) || (b == enChar.length - 3)) {
                        end = 2;

                    }

                    // if ((i == 0 || b == 0) && i == (pasChar.length - 1) || (b == enChar.length -
                    // * 1)) { starter = 0; end = 0; } if ((i == 0 || b == 0) && i ==
                    // (pasChar.length
                    // * - 2) || (b == enChar.length - 2)) { starter = 0; end = 1; } if ((i == 0 ||
                    // b
                    // * == 0) && i == (pasChar.length - 3) || (b == enChar.length - 3)) { starter =
                    // * 0; end = 2; }
                    // *
                    // * if ((i == 1 || b == 1) && i == (pasChar.length - 1) || (b == enChar.length
                    // -
                    // * 1)) { starter = -1; end = 0; } if ((i == 1 || b == 1) && i ==
                    // (pasChar.length
                    // * - 2) || (b == enChar.length - 2)) { starter = -1; end = 1; } if ((i == 1 ||
                    // b
                    // * == 1) && i == (pasChar.length - 3) || (b == enChar.length - 3)) { starter =
                    // * -1; end = 2; }

                    // determines which previous and next pairs are also matches
                    int commonCounter = 0;
                    for (int n = starter; n < end; n++) {
                        if (pasChar[b + n] == enChar[i + n]) {
                            commonCounter++;
                        }
                    }
                    if (commonCounter >= 3) {
                        System.out.print("Q");
                    } else {
                        System.out.print("x");
                    }

                } else {
                    System.out.print(".");
                }
                b++;
            }

            i++;
        }

    }
}
